﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.Structures
{
    public class FolderStructure
    {
        public List<FolderStructure> Children { get; set; }
        public FolderStructure Parent { get; set; }

        public string Name { get; set; }
        public List<FileStructure> Files { get; set; }

        public FolderStructure(string name, List<FolderStructure> children, FolderStructure parent, List<FileStructure> files)
        {
            Name = name;
            Children = children;
            Parent = parent;
            Files = files;
        }

        public int GetSizeOfFolder()
        {
            int size = 0;

            
            foreach (FolderStructure child in Children)
            {
               size += child.GetSizeOfFolder();
            }
            foreach(FileStructure file in Files)
            {
                size += file.GetSizeOfFile();
            }

            return size;
        }
    }
}
