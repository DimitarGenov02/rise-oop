﻿using OOPFileSystem.Structures;

namespace OOPFileSystem.FoldersModels
{
    public class FolderFunctions
    {
        public static string Create(FolderStructure parent, string folderName) 
        {
            if(folderName == "" || folderName.Contains('/'))
            {
                return ">Invalid name";
            }

            if (Exist.Folder(folderName, parent))
                return "Folder already exist";

            List<FolderStructure> children = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure(folderName, children, parent,files);
            parent.Children.Add(folder);

            return ">Folder created successfully";
        }
        public static FolderStructure MoveLocation(FolderStructure current, string path, ref bool invalid) 
        {
            if(path == ".." && current.Parent != null)
            {
                return current.Parent;
            }

            foreach(FolderStructure folder in current.Children)
            {
                if(folder.Name == path) 
                {
                    return folder;
                }
            }
            invalid = true;
            return current;
        }
    }
}
