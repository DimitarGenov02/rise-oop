﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.FilesModels
{
    public abstract class WCFunction
    {
        public static string WCFunctionTextImplementation(string input)
        {
            input = input.Trim();
            int count_words = input.Split(' ').Length;
            return "Number of words: " + count_words.ToString();
        }

        public static string WCFunctionLine(string input)
        {
            input = input.Trim();
            int count_lines = input.Split('\n').Length;
            return "Number of lines: " + count_lines.ToString();
        }
    }
}
