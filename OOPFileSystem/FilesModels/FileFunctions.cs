﻿using OOPFileSystem.Structures;

namespace OOPFileSystem.FilesModels
{
    public class FileFunctions
    {
        public static string Touch(FolderStructure folder, string fileName)
        {
            if (fileName == "" || fileName.Contains('/'))
                return ">Invalid name";

            if (Exist.File(fileName, folder))
                return ">File already exist";

            folder.Files.Add(new FileStructure(fileName, new Dictionary<int, string>()));

            return ">File created successfully";
        }

        public static string Write(FolderStructure folder, string fileName, string number, string content) 
        {
            if(!int.TryParse(number, out int value)) 
                return ">Invalid line"; 

            if (!Exist.File(fileName, folder))
                return ">File not exist";

            FileStructure file = Exist.GetFileFromName(fileName, folder);
            if (file.ContentOfFile.ContainsKey(value))
            {
                file.ContentOfFile.Remove(value);
            }
            file.ContentOfFile.Add(value, content);

            return ">Line Added";
        }

        public static int WordCount(FolderStructure currentPath,string fileName)
        {
            int counter = 0;
            fileName.Trim();
            if(Exist.File(fileName, currentPath))
            {
                FileStructure file = Exist.GetFileFromName(fileName, currentPath);

                foreach(string line in file.ContentOfFile.Values)
                {
                    string[] array = line.Split(' ');
                    counter += array.Length;
                }

                return counter;
            }
            string[] txt = fileName.Split(' ');

            return txt.Length - 1;
        }
    }
}
