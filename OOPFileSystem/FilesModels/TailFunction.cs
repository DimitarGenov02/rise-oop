﻿using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystem.FilesModels
{
    public class TailFunction
    {
        public static string ShowTailOfFile(FileStructure file, int option)
        {   
            if (file.ContentOfFile.Count == 0) return $"File {file.FileName} is Empty\n";

            int ind_of_last_line = file.ContentOfFile.Keys.Max();
             
            if(option == 0)option = 10 ;

            if (ind_of_last_line <= 10 && option == 0) option = ind_of_last_line ;
            
            string result = $"Last {option} lines from {file.FileName}:\n";

            if (option > ind_of_last_line) {
                if (ind_of_last_line >= 10)
                {
                    option = 10;
                    result = $"Not enough lines. Here are the last 10 lines from {file.FileName}:\n";
                }
                else { 
                    option = ind_of_last_line;
                    result = $"Not enough lines. Here are the last {option} lines from {file.FileName}:\n";
                }
            }

                for (int i = ind_of_last_line - option + 1 ; i <= ind_of_last_line; i++)
                {
                    if (file.ContentOfFile.ContainsKey(i)) result += file.ContentOfFile[i].ToString() + "\n";
                    else result += "\n";
                }

            return result ;
        }
    }
}
