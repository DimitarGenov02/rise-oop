﻿namespace OOPFileSystem
{
    public class Name
    {
        public static string Merge(string[] words, int start)
        {
            string result = string.Empty;

            for (int i = start;i < words.Length; i++)
            {
                result += words[i] + " ";
            }

            return result.Trim();
        }


    }
}
