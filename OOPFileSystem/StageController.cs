﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.FoldersModels;
using OOPFileSystem.PipeLineOptions;
using OOPFileSystem.Structures;

namespace OOPFileSystem
{
    public class StageController
    {
        public static void Controller()
        {
            FolderStructure currentPath = InitRoot();
            bool flagWork = true;

            while (flagWork)
            {
                Console.Write(">{0} ", currentPath.Name);

                string input = Console.ReadLine() + " ";
                flagWork = Commands(ref currentPath, input);
            }
        }

        public static FolderStructure InitRoot()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure root = new FolderStructure("home", folders, null!, file);

            return root;
        }

        public static bool Commands(ref FolderStructure currentPath, string input)
        {
            if (PipeLineFunctions.hasPipeLine(input))
            {
                Console.WriteLine(PipeLineFunctions.PipeLineInplementation(input, currentPath));
                return true;
            }
            string[] command = input.Split(' ');
            string name;

            switch (command[0])
            {
                case "mkdir":
                    name = Name.Merge(command, 1);
                    Console.WriteLine(FolderFunctions.Create(currentPath, name));
                    break;
                case "cd":
                    bool invalid = false;
                    name = Name.Merge(command, 1);
                    currentPath = FolderFunctions.MoveLocation(currentPath, name, ref invalid);
                    if (invalid)
                    {
                        Console.WriteLine("Invalid path");
                    }
                    break;
                case "touch":
                    name = Name.Merge(command, 1);
                    Console.WriteLine(FileFunctions.Touch(currentPath, name));
                    break;
                case "write":
                    if (command.Length < 5)
                    {
                        Console.WriteLine("Invalid command!");
                        break;
                    }
                    name = Name.Merge(command, 3);
                    Console.WriteLine(FileFunctions.Write(currentPath, command[1], command[2], name));
                    break;
                case "wc":
                    name = Name.Merge(command, 1);
                    Console.WriteLine(FileFunctions.WordCount(currentPath, name));
                    break;
                case "ls":
                    if (command[1] == "--sorted") Console.WriteLine(LsFunction.ShowContentOfFolderSortedDecending(currentPath));
                    else Console.WriteLine(LsFunction.ShowContentOfFolder(currentPath));
                    break;
                case "cat":
                    if (Exist.File(command[1], currentPath))
                    {
                        FileStructure foundFile = Exist.GetFileFromName(command[1], currentPath);
                        Console.WriteLine(CatFunction.ShowContentOfFile(foundFile));
                    }
                    break;
                case "tail":
                    if (Exist.File(command[1], currentPath))
                    {
                        FileStructure foundFile = Exist.GetFileFromName(command[1], currentPath);
                        Console.WriteLine(command.Length == 5
                            ? TailFunction.ShowTailOfFile(foundFile, int.Parse(command[3]))
                            : TailFunction.ShowTailOfFile(foundFile, 0));
                    }
                    break;
                case "exit":
                    if (command[0] == "exit")
                    {
                        return false;
                    }
                    break;
                default:
                    Console.WriteLine("Invalid command");
                    break;
            }
            return true;
        }
    }
}
