﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatureReserveSimulation.AnimalSpecies;

namespace NatureReserveSimulation.OtherFoods
{
    public class Meat : IFood
    {
        public int GetNutrition()
        {
            return 5;
        }

        public string GetTypeOfObject()
        {
            return GetType().ToString().Split('.').Last();
        }

        public void SetNutritionZero()
        {
            throw new NotImplementedException();
        }
    }
}
