﻿using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.Biomes;
using NatureReserveSimulation.Factories;
using NatureReserveSimulation.Species;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation
{
    public class BiomeController
    {
        public static void RunBiomeSimulation()
        {
            Biome[,] map = new Biome[,] {
               { BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains") },
               { BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains") },
               { BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains") },
               { BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains"), BiomeFactory.Create("Plains") }

            };

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    
                    foreach(Animal animal in map[i, j].Animals_in_biome)
                    {
                        if (animal.isStarving()) TryMove();
                    }
                }
                
            }

            Console.WriteLine("Success");
        }

        private static void TryMove()
        {
            throw new NotImplementedException();
        }
    }
}
