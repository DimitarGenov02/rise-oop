﻿using NatureReserveSimulation;

internal class Program
{
    private static void Main(string[] args)
    {
        // StageController.StageControllerMethod();
        BiomeController.RunBiomeSimulation();
    }
}