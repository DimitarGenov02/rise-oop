﻿using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.OtherFoods;
using NatureReserveSimulation.PlantSpecies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Factories
{
    public class FoodFactory
    {
        public static IFood Create(string type_of_food)
        {
            switch (type_of_food)
            {
                case "Carrot":
                    return new Carrot(5);
                case "Fruits":
                    return new Fruits(7);
                case "Grass":
                    return new Grass(3);
                case "Lettuce":
                    return new Lettuce(2);
                case "Meat":
                    return new Meat();
                case "Berries":
                    return new Berries(5);
                default:
                    return null;

            }
        }
    }
}
