﻿using NatureReserveSimulation.Biomes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Factories
{
    public class BiomeFactory
    {
        public static Biome Create(string type_of_biome)
        {
            switch (type_of_biome) {

                case "Plains":
                    return new Plains(10);
                    
                default:return null;
            
            };
        }
    }
}
