﻿using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.Species;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Factories
{
    public class DietFactory
    {
        public static HashSet<IFood> Create(string type)
        {
            switch (type)
            {
                case "Bear":
                    return new HashSet<IFood>() { FoodFactory.Create("Fruits"), FoodFactory.Create("Meat"), FoodFactory.Create("Berries") };
                case "Wolf":
                    return new HashSet<IFood>() { FoodFactory.Create("Meat") };
                case "Rabbit":
                    return new HashSet<IFood>() { FoodFactory.Create("Lettuce"), FoodFactory.Create("Carrot") };
                case "Pig":
                    return new HashSet<IFood>() { FoodFactory.Create("Fruits"), FoodFactory.Create("Lettuce") };
                case "Deer":
                    return new HashSet<IFood>() { FoodFactory.Create("Berries"), FoodFactory.Create("Grass") };
                case "Cow":
                    return new HashSet<IFood>() { FoodFactory.Create("Grass") };
                default: return null;
            }
        }
    }
}
