﻿using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.Species;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Factories
{
    public class AnimalFactory
    {
        public static Animal Create(string animal, HashSet<IFood> diet)
        {
            switch (animal)
            {
                case "Bear":
                    return new Bear(10, diet, 4, false);
                case "Wolf":
                    return new Wolf(7, diet, 2, false);
                case "Rabbit":
                    return new Rabbit(4, diet, 2, true);
                case "Pig":
                    return new Pig(6, diet, 3, true);
                case "Deer":
                    return new Deer(7, diet, 3, true);
                case "Cow":
                    return new Cow(8, diet, 3, true);
                default:
                    return null;
            }
        }
    }
}
