﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatureReserveSimulation.AnimalSpecies;

namespace NatureReserveSimulation.PlantSpecies
{
    public interface IPlant
    {
        void RegenerateNutrition();
        
    }

    public abstract class Plant : IPlant,IFood
    {
        private readonly int maxNutrition;
        private int currentNutrition;
        private int regenCoefficient;

        public Plant(int maxNutrition) { 
            this.maxNutrition = maxNutrition;
            currentNutrition = maxNutrition;
            regenCoefficient = SetRegenCoefficient();
        }

        public bool IsEaten()
        {
            return currentNutrition == 0;
        }
        public void RegenerateNutrition()
        {
            if (currentNutrition + regenCoefficient > maxNutrition) currentNutrition = maxNutrition;
            else if (IsEaten()) currentNutrition += regenCoefficient;
            else currentNutrition += regenCoefficient;
        }
        public int GetNutrition()
        {
            return currentNutrition;
        }


        public abstract int SetRegenCoefficient();

        public void SetNutritionZero()
        {
            currentNutrition = 0;
        }

        public string GetTypeOfObject()
        {
            return GetType().ToString().Split('.').Last();
        }

        public override bool Equals(object? obj)
        {
           if(obj == null || GetType() != obj.GetType())
            {
                return false;
            }
           IFood food = obj as IFood;
            return food.GetType() == obj.GetType();
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + GetType().GetHashCode();
            return hash;
        }

    }
}
