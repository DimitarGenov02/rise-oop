﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.PlantSpecies
{
    public class Berries : Plant
    {
        public Berries(int maxNutrition) : base(maxNutrition)
        {
        }
        public override int SetRegenCoefficient()
        {
            return 3;
        }
    }
}
