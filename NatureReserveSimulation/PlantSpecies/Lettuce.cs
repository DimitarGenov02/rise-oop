﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.PlantSpecies
{
    public class Lettuce : Plant
    {
        public Lettuce(int maxNutrition) : base(maxNutrition)
        {
        }

        public override int SetRegenCoefficient()
        {
            return 2;
        }
    }
}
