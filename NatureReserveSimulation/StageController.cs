﻿using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.Factories;
using NatureReserveSimulation.PlantSpecies;
using NatureReserveSimulation.Species;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation
{
    public class StageController
    {
        public static void StageControllerMethod() {

        List<int> BearStatistic = new List<int>();
        List<int> CowStatistic = new List<int>();
        List<int> DeerStatistic = new List<int>();
        List<int> PigStatistic = new List<int>();
        List<int> RabbitStatistic = new List<int>();
        List<int> WolfStatistic = new List<int>();

            
        

        HashSet<IFood> bear_diet = new HashSet<IFood>() { FoodFactory.Create("Fruits"), FoodFactory.Create("Meat"), FoodFactory.Create("Berries") };
        HashSet<IFood> rabbit_diet = new HashSet<IFood>() { FoodFactory.Create("Lettuce"), FoodFactory.Create("Carrot") };
        HashSet<IFood> cow_diet = new HashSet<IFood>() { FoodFactory.Create("Grass") };
        HashSet<IFood> wolf_diet = new HashSet<IFood>() { FoodFactory.Create("Meat") };
        HashSet<IFood> pig_diet = new HashSet<IFood>() { FoodFactory.Create("Fruits"), FoodFactory.Create("Lettuce") };
        HashSet<IFood> deer_diet = new HashSet<IFood>() { FoodFactory.Create("Berries"), FoodFactory.Create("Grass") };


            Animal bear_1 = new Bear(10, bear_diet, 4, false);
            Animal bear_2 = new Bear(10, bear_diet, 4, false);
            Animal bear_3 = new Bear(10, bear_diet, 4, false);
            Animal bear_4 = new Bear(10, bear_diet, 4, false);


            Animal rabbit_1 = new Rabbit(10, rabbit_diet, 3, true);
            Animal rabbit_2 = new Rabbit(10, rabbit_diet, 3, true);
            Animal rabbit_3 = new Rabbit(10, rabbit_diet, 3, true);
            Animal rabbit_4 = new Rabbit(10, rabbit_diet, 3,true);

            Animal cow_1 = new Cow(10, cow_diet, 4,true);
            Animal cow_2 = new Cow(10, cow_diet, 4, true);
            Animal cow_3 = new Cow(10, cow_diet, 4, true);
            Animal cow_4 = new Cow(10, cow_diet, 4, true);

            Animal wolf_1 = new Wolf(10, wolf_diet, 3, false);
            Animal wolf_2 = new Wolf(10, wolf_diet, 3, false);
            Animal wolf_3 = new Wolf(10, wolf_diet, 3, false);
            Animal wolf_4 = new Wolf(10, wolf_diet, 3, false);

            Animal pig_1 = new Pig(10, pig_diet, 4, true);
            Animal pig_2 = new Pig(10, pig_diet, 4, true);
            Animal pig_3 = new Pig(10, pig_diet, 4, true);
            Animal pig_4 = new Pig(10, pig_diet, 4, true);

            Animal deer_1 = new Deer(10, deer_diet, 4, true);
            Animal deer_2 = new Deer(10, deer_diet, 4, true);
            Animal deer_3 = new Deer(10, deer_diet, 4, true);
            Animal deer_4 = new Deer(10, deer_diet, 4, true);

            //for every biome
            
            HashSet<Animal> animals = new HashSet<Animal>() {
                pig_1, pig_2, pig_3, pig_4,
                deer_1, deer_2, deer_3, deer_4,
                wolf_1, wolf_2, wolf_3, wolf_4,
                cow_1, cow_2, cow_3, cow_4,
                rabbit_1, rabbit_2, rabbit_3, rabbit_4,
                bear_1, bear_2, bear_3, bear_4
            };
            
            /*
            HashSet<Animal> animals = new HashSet<Animal>() {
                pig_1, pig_2, pig_3, pig_4,
                bear_1,  bear_4
            };*/

            //for every tile
            HashSet<IFood> foods = new HashSet<IFood>() {FoodFactory.Create("Grass"), FoodFactory.Create("Berries") };
            HashSet<Animal> list_alive_animals = new HashSet<Animal>();
            //for every tile


            foreach (Animal animal in animals)
            {
                if (animal.isHerbivor()) foods.Add(animal);
                list_alive_animals.Add(animal);
            }

           

            int stageCounter = 1;
            Random random = new Random();
            
            //for every tile
            while (animals.Count > 0)
            {
                Console.WriteLine("Stage: " + stageCounter);
                foreach (Animal animal in animals)
                {

                    if (animal.isKilled() || animal.isDead()) continue;
                    if (animal.isAdult(stageCounter))
                    {
                        switch (animal.isHerbivor())
                        {
                            case true:
                                
                                break;
                            case false:
                                foreach(Animal to_diet in animals)
                                {
                                    if (to_diet.isHerbivor()) animal.Diet.Add(to_diet);
                                }
                                break;
                        }
                    }
                    IFood possible_food = foods.ElementAt(random.Next(foods.Count));
                    Console.WriteLine("Animal: " + animal.GetTypeOfObject());
                    Console.WriteLine("Current energy: " + animal.CurrentEnergy);
                    Console.WriteLine("Food: " + possible_food.GetTypeOfObject());
                    Console.WriteLine("Can eat: " + animal.Diet.Contains(possible_food).ToString());
                    Console.WriteLine("Can it be killed: " + list_alive_animals.Contains(possible_food).ToString());
                    Console.WriteLine();

                    switch (possible_food)
                    {
                        case Animal:
                            
                            if (list_alive_animals.Contains(possible_food) && animal.Diet.Contains(possible_food))
                            {
                                Animal? killed_animal = animals?.FirstOrDefault(animal => animal.Equals(possible_food) && !animal.isKilled());
                                if (killed_animal != null) {
                                    killed_animal.SetDead();
                                    animal.Feed(possible_food);
                                }
                                
                               // list_alive_animals.Remove(possible_food);
                                Console.WriteLine("\nEaten :" + possible_food.GetTypeOfObject() + "\n");
                            }
                            else animal.Feed(given_food: null);
                            break;
                        case Plant:
                            animal.Feed(possible_food);
                            if (animal.Diet.Contains(possible_food))
                            {
                                possible_food.SetNutritionZero();
                            }
                            Plant plant = possible_food as Plant;
                            plant.RegenerateNutrition();
                            break;
                    }



                }

                Console.WriteLine("--------------------------");
                
                foreach (Animal animal in animals)
                {
                    
                    if (animal.isDead() || animal.isKilled())
                    {
                        switch (animal)
                        {
                            case Bear:
                                BearStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                            case Wolf:
                                WolfStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                            case Cow:
                                CowStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                            case Deer:
                                DeerStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                            case Rabbit:
                                RabbitStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                            case Pig:
                                PigStatistic.Add(stageCounter);
                                list_alive_animals.Remove(animal);
                                break;
                        }
                        animals.Remove(animal);
                    }
                }
                stageCounter++;
               
            }
            
            //Up from here needs work

            Console.WriteLine("Rabbit Max Lifespan: " + RabbitStatistic.Max());
            Console.WriteLine("Rabbit Min lifespan: " + RabbitStatistic.Min());
            Console.WriteLine("Rabbit Average lifespan: "+ RabbitStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Wolf Max Lifespan: " + WolfStatistic.Max());
            Console.WriteLine("Wolf Min lifespan: " + WolfStatistic.Min());
            Console.WriteLine("Wolf Average lifespan: " + WolfStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Pig Max Lifespan: " + PigStatistic.Max());
            Console.WriteLine("Pig Min lifespan: " + PigStatistic.Min());
            Console.WriteLine("Pig Average lifespan: " + PigStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Deer Max Lifespan: " + DeerStatistic.Max());
            Console.WriteLine("Deer Min lifespan: " + DeerStatistic.Min());
            Console.WriteLine("Deer Average lifespan: " + DeerStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Cow Max Lifespan: " + CowStatistic.Max());
            Console.WriteLine("Cow Min lifespan: " + CowStatistic.Min());
            Console.WriteLine("Cow Average lifespan: " + CowStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Bear Max Lifespan: " + BearStatistic.Max());
            Console.WriteLine("Bear Min lifespan: " + BearStatistic.Min());
            Console.WriteLine("Bear Average lifespan: " + BearStatistic.Average());
            Console.WriteLine();
            Console.WriteLine("Success");
        }
        
        
    }
}
