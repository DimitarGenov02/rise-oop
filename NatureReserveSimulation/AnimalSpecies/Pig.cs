﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatureReserveSimulation.AnimalSpecies;

namespace NatureReserveSimulation.Species
{
    public class Pig : Animal
    {
        public Pig(int maximumEnergy, HashSet<IFood> diet, int adultAge, bool isHerbivor) 
            : base(maximumEnergy, diet, adultAge, isHerbivor) { }
       
        public override string MakeSound()
        {
            throw new NotImplementedException();
        }


        public override int GetStarvingCoefficient()
        {
            return 3;
        }
    }
}
