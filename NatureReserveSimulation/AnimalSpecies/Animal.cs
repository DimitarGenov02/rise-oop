﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.AnimalSpecies
{

    public interface IAnimal
    {
        string MakeSound();
        bool isStarving();
        bool isAdult(int stage);
        int GetStarvingCoefficient();
    }

    public interface IFood
    {
        string GetTypeOfObject();
        int GetNutrition();
        void SetNutritionZero();
    }

    public abstract class Animal : IAnimal, IFood
    {

        private readonly int maximumEnergy;
        private int currentEnergy;
        private HashSet<IFood>? diet;
        private int adultAge;
        private bool herbivor;
        private bool killed;


        public Animal(int maximumEnergy, HashSet<IFood> diet, int adultAge, bool isHerbivor)
        {
            this.maximumEnergy = maximumEnergy;
            this.diet = diet;
            currentEnergy = maximumEnergy;
            this.adultAge = adultAge;
            herbivor = isHerbivor;
            killed = false;
        }


        public HashSet<IFood> Diet
        {
            get { return diet; }
            set { diet = value; }
        }

        public int CurrentEnergy
        {
            get { return currentEnergy; }
            set { currentEnergy = value; }
        }

        public bool isDead()
        {
            return currentEnergy == 0;
        }

        public bool isKilled()
        {
            return killed;
        }

        public bool isHerbivor()
        {
            return herbivor;
        }

        public void SetDead()
        {
            killed = true;
        }

        public void Feed(IFood given_food)
        {
            switch (diet.Contains(given_food), isStarving())
            {
                case (true, false):
                    if (currentEnergy + given_food.GetNutrition() >= maximumEnergy) currentEnergy = maximumEnergy;
                    else currentEnergy += given_food.GetNutrition();
                    break;
                case (true, true):
                    if (currentEnergy + given_food.GetNutrition() >= maximumEnergy) currentEnergy = maximumEnergy;
                    else currentEnergy += given_food.GetNutrition();
                    break;
                case (false, true):
                    if (currentEnergy <= GetStarvingCoefficient()) currentEnergy = 0;
                    else currentEnergy -= GetStarvingCoefficient();
                    break;
                case (false, false):
                    currentEnergy--;
                    break;
                default:
                    break;

            }
        }
        public string GetTypeOfObject()
        {
            return GetType().ToString().Split('.').Last();
        }
        public int GetNutrition()
        {
            return currentEnergy;
        }
        public bool isStarving()
        {
            if (currentEnergy <= maximumEnergy / 2) return true;
            else return false;
        }
        public bool isAdult(int stage)
        {
            if (adultAge <= stage) return true;
            return false;
        }

        public override bool Equals(object? obj)
        {
            return GetType() == obj.GetType();
        }
        /*
        public override int GetHashCode()
        {
            return GetType().GetHashCode();
        }
        */
        public abstract int GetStarvingCoefficient();
        public abstract string MakeSound();
        public void SetNutritionZero()
        {
            throw new NotImplementedException();
        }
    }
}
