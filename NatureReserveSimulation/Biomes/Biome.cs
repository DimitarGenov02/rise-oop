﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatureReserveSimulation.AnimalSpecies;

namespace NatureReserveSimulation.Biomes
{
    public interface IBiome
    {
        HashSet<IFood> foods_in_biome();
        HashSet<Animal> possible_species();
    }

    public abstract class Biome : IBiome
    {
        private int maxquantityanimals;
        private int currentQuantity;
        private HashSet<IFood> possible_foods;
        private HashSet<Animal> species;
        private HashSet<Animal> animals_in_biome;

        public Biome(int maxquantityanimals)
        {
            this.maxquantityanimals = maxquantityanimals;
            possible_foods = foods_in_biome();
            species = possible_species();
            currentQuantity = 0;
        }
        public HashSet<Animal> Species { get { return species; } }
        public HashSet<IFood> Possible_foods
        {
            get { return possible_foods; }
            set { possible_foods = value; }
        }

        public HashSet<Animal> Animals_in_biome
        {
            get { return  animals_in_biome; }
            set { animals_in_biome = value; }
        }

        public int CurrentQuantity
        {
            get { return currentQuantity; }
            set { currentQuantity = value; }
        }

        public bool IsFull()
        {
            return currentQuantity == maxquantityanimals;
        }

        public override bool Equals(object? obj)
        {
            return GetType() == obj.GetType();
        }
        public abstract HashSet<IFood> foods_in_biome();
        public abstract HashSet<Animal> possible_species();
        
    }
}
