﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NatureReserveSimulation.AnimalSpecies;
using NatureReserveSimulation.Factories;

namespace NatureReserveSimulation.Biomes
{
    public class Plains : Biome
    {
        public Plains(int quantityanimals) : base(quantityanimals)
        {
        }

        public override HashSet<IFood> foods_in_biome()
        {
            return new HashSet<IFood>() { 
                FoodFactory.Create("Grass"),
                FoodFactory.Create("Lettuce")
            };
        }

        public override HashSet<Animal> possible_species()
        {
            return new HashSet<Animal>() { 
                AnimalFactory.Create("Cow",DietFactory.Create("Cow")),
                AnimalFactory.Create("Wolf",DietFactory.Create("Wolf")),
                AnimalFactory.Create("Rabbit", DietFactory.Create("Rabbit"))
            };
        }
    }
}
