﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class TailFunctionTests
    {
        [TestMethod]
        public void ShowTailProperlyTest10Lines()
        {
            //Arrange
            Dictionary<int,string> content = new Dictionary<int,string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");
            file.ContentOfFile.Add(10, "deseti red");
            //Act
            string result = TailFunction.ShowTailOfFile(file,0);

            //Assert
            Assert.AreEqual("Last 10 lines from filche:\n\nvtori red\n\n\n\n\n\nosmi red\n\ndeseti red\n", result);
        }

        [TestMethod]
        public void ShowTailProperlyTest10LinesWithOption()
        {
            //Arrange
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");
            file.ContentOfFile.Add(10, "deseti red");
            //Act
            string result = TailFunction.ShowTailOfFile(file, 4);

            //Assert
            Assert.AreEqual("Last 4 lines from filche:\n\nosmi red\n\ndeseti red\n", result);
        }

        [TestMethod]
        public void ShowTailProperlyTestLessThan10LineswithOption()
        {
            //Arrange
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");
            
            //Act
            string result = TailFunction.ShowTailOfFile(file, 3);

            //Assert
            Assert.AreEqual("Last 3 lines from filche:\n\n\nosmi red\n", result);
        }

        [TestMethod]
        public void ShowTailProperlyTestMoreThan10Lines()
        {
            //Arrange
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");
            file.ContentOfFile.Add(10, "deseti red");
            file.ContentOfFile.Add(12, "dvanadeseti red");
            //Act
            string result = TailFunction.ShowTailOfFile(file, 0);

            //Assert
            Assert.AreEqual("Last 10 lines from filche:\n\n\n\n\n\nosmi red\n\ndeseti red\n\ndvanadeseti red\n", result);
        }

        [TestMethod]
        public void ShowTailProperlyTestMoreThan10BadOption()
        {
            //Arrange
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");
            file.ContentOfFile.Add(10, "deseti red");
            file.ContentOfFile.Add(12, "dvanadeseti red");
            //Act
            string result = TailFunction.ShowTailOfFile(file, 14);

            //Assert
            Assert.AreEqual("Not enough lines. Here are the last 10 lines from filche:\n\n\n\n\n\nosmi red\n\ndeseti red\n\ndvanadeseti red\n", result);
        }

        [TestMethod]
        public void ShowTailProperlyTestLessThan10BadOption()
        {
            //Arrange
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("filche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(8, "osmi red");

            //Act
            string result = TailFunction.ShowTailOfFile(file, 10);

            //Assert
            Assert.AreEqual("Not enough lines. Here are the last 8 lines from filche:\n\nvtori red\n\n\n\n\n\nosmi red\n", result);
        }

        [TestMethod]
        public void ShowTailEmpty()
        {
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("kratkotekstche", content);


            //Act
            string result = TailFunction.ShowTailOfFile(file, 0);

            //Assert
            Assert.AreEqual("File kratkotekstche is Empty\n", result);
        }
    }
}
