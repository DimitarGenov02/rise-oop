﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class CatFunctionTests
    {
        [TestMethod]
        public void CatFunctionShowingProperly1() {
            //Arrange
            Dictionary<int,string> content = new Dictionary<int,string>();
            FileStructure file = new FileStructure("kratkotekstche",content);

            file.ContentOfFile.Add(1, "purvi red");
            file.ContentOfFile.Add(5, "peti red");
            //Act
            string result = CatFunction.ShowContentOfFile(file);

            //Assert
            Assert.AreEqual("kratkotekstche:\npurvi red\n\n\n\npeti red\n", result);
        }

        [TestMethod]
        public void CatFunctionShowingProperly2()
        {
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("kratkotekstche", content);

            file.ContentOfFile.Add(2, "vtori red");
            file.ContentOfFile.Add(7, "sedmi red");
            //Act
            string result = CatFunction.ShowContentOfFile(file);

            //Assert
            Assert.AreEqual("kratkotekstche:\n\nvtori red\n\n\n\n\nsedmi red\n", result);
        }

        [TestMethod]
        public void CatFunctionShowingProperly3()
        {
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("kratkotekstche", content);

            file.ContentOfFile.Add(1, "purvi red");
            file.ContentOfFile.Add(5, "peti red");
            file.ContentOfFile.Add(6, "shesti red");
            file.ContentOfFile.Add(10, "deseti red");
            //Act
            string result = CatFunction.ShowContentOfFile(file);

            //Assert
            Assert.AreEqual("kratkotekstche:\npurvi red\n\n\n\npeti red\nshesti red\n\n\n\ndeseti red\n", result);
        }

        [TestMethod]
        public void CatFunctionShowingEmpty()
        {
            Dictionary<int, string> content = new Dictionary<int, string>();
            FileStructure file = new FileStructure("kratkotekstche", content);

            
            //Act
            string result = CatFunction.ShowContentOfFile(file);

            //Assert
            Assert.AreEqual("File kratkotekstche is Empty\n", result);
        }
    }
}
