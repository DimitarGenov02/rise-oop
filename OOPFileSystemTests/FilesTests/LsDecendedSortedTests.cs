﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class LsDecendedSortedTests
    {
        [TestMethod]
        public void Test1()
        {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();


            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder2.Files.Add(new FileStructure("banan", new Dictionary<int, string> { { 1, "Proba" }, { 2, "Super" } }));
            folder2.Files.Add(new FileStructure("honda", new Dictionary<int, string> { { 1, "dve" }, { 2, "opa" } }));
            folder.Children.Add(folder2);

            //Act
            string result =LsFunction.ShowContentOfFolderSortedDecending(folder);


            //Assert
            Assert.AreEqual("Files in folder_for_test sorted by size:\ncontent_folders - Folder - 20 bytes\n", result);
        }

        [TestMethod]
        public void Test2()
        {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();


            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder2.Files.Add(new FileStructure("banan", new Dictionary<int, string> { { 1, "Proba" }, { 2, "Super" } }));
            folder2.Files.Add(new FileStructure("honda", new Dictionary<int, string> { { 1, "dve" }, { 2, "opa" } }));
            folder.Children.Add(folder2);

            //Act
            string result = LsFunction.ShowContentOfFolderSortedDecending(folder2);


            //Assert
            Assert.AreEqual("Files in content_folders sorted by size:\nbanan - File - 12 bytes\nhonda - File - 8 bytes\n", result);
        }

        [TestMethod]
        public void ShowDescendedEmpty()
        {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);

            //Act
            string result = LsFunction.ShowContentOfFolderSortedDecending(folder);

            //Assert
            Assert.AreEqual("Files in folder_for_test sorted by size:\nNone - 0 bytes\n", result);
        }
    }
}
