﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class LsFunctionTests
    {
        [TestMethod]
        public void ShowContentProperly1()
        {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();
            

            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder2.Files.Add(new FileStructure("banan",new Dictionary<int, string> { { 1, "Proba" },{2,"Super" } }));
            folder2.Files.Add(new FileStructure("honda", new Dictionary<int, string> { { 1, "dve" }, { 2, "opa" } }));
            folder.Children.Add(folder2);

            //Act
            string result = LsFunction.ShowContentOfFolder(folder);


            //Assert
            Assert.AreEqual("Files in folder_for_test:\ncontent_folders - Folder\n", result);
        }

        [TestMethod]
        public void ShowContentProperly2()
        {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();
            files2.Add(new FileStructure("banan", null));
            files2.Add(new FileStructure("sila", null));

            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder.Children.Add(folder2);
            //Act
            string result = LsFunction.ShowContentOfFolder(folder2);


            //Assert
            Assert.AreEqual("Files in content_folders:\nbanan - File\nsila - File\n", result);
        }

        [TestMethod]
        public void ShowContentProperly3() {
            //Arrange
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();
            files2.Add(new FileStructure("banan", null));
            files2.Add(new FileStructure("sila", null));

            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder.Children.Add(folder2);
            files.Add(new FileStructure("failche", null));
            //Act
            string result = LsFunction.ShowContentOfFolder(folder);


            //Assert
            Assert.AreEqual("Files in folder_for_test:\ncontent_folders - Folder\nfailche - File\n", result);

        }

        [TestMethod]
        public void ShowContentEmpty()
        {
            List<FolderStructure> folders = new List<FolderStructure>();
            List<FileStructure> files = new List<FileStructure>();
            FolderStructure folder = new FolderStructure("folder_for_test", folders, null, files);


            List<FolderStructure> folders2 = new List<FolderStructure>();
            List<FileStructure> files2 = new List<FileStructure>();
            //files2.Add(new FileStructure("banan", null));
            //files2.Add(new FileStructure("sila", null));

            FolderStructure folder2 = new FolderStructure("content_folders", folders2, folder, files2);

            folder.Children.Add(folder2);
            //Act
            string result = LsFunction.ShowContentOfFolder(folder2);


            //Assert
            Assert.AreEqual("Files in content_folders:\nNone\n", result);
        }
    }
}
