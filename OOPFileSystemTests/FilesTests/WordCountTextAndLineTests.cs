﻿using OOPFileSystem.FilesModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class WordCountTextAndLineTests
    {
        [TestMethod]
        public void WcTextTest()
        {
            //Arrange
            string test = "this is just random text.";
            
            //Act
            string result = WCFunction.WCFunctionTextImplementation(test);

            //Assert
            Assert.AreEqual("Number of words: 5", result);
        }

        [TestMethod]
        public void WcLineTest() {
            //Arrange
            string test = "this is just random text. " +
                          "with multiple lines" +  
                          "maybe?";

            //Act
            string result = WCFunction.WCFunctionLine(test);

            //Assert
            Assert.AreEqual("Number of lines: 1", result);
        }

        [TestMethod]
        public void WcLineTest2() {
            string test = "this is just random text.\n" +
                              "with multiple lines\n" +
                              "maybe?";

            //Act
            string result = WCFunction.WCFunctionLine(test);

            //Assert
            Assert.AreEqual("Number of lines: 3", result);
        }

        [TestMethod]
        public void WcTextTest2() {
            string test = "this is just random text. " +
                              "with multiple lines " +
                              "maybe?";

            //Act
            string result = WCFunction.WCFunctionTextImplementation(test);

            //Assert
            Assert.AreEqual("Number of words: 9", result);
        }
    }
}
