﻿using OOPFileSystem.FilesModels;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.FilesTests
{
    [TestClass]
    public class FileFunctionTests
    {
        [TestMethod]

        public void TestCreateFile()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Touch(currentPath, "file.txt");

            Assert.AreEqual(expected, ">File created successfully");
        }

        [TestMethod]

        public void TestCreateFileInvalidName() 
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Touch(currentPath, "/file.txt");

            Assert.AreEqual(expected, ">Invalid name");
        }

        [TestMethod]
        public void TestCreateFileEmptyName()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Touch(currentPath, "");

            Assert.AreEqual(expected, ">Invalid name");
        }

        [TestMethod]

        public void TestWriteInFile() 
        {
            Dictionary<int, string> lines = new Dictionary<int, string>();
            FileStructure fileStructure = new FileStructure("file", lines);
            List<FileStructure> file = new List<FileStructure>() { fileStructure };
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Write(currentPath, "file", "4", "hah");

            Assert.AreEqual(expected, ">Line Added");
        }

        [TestMethod]

        public void TestWriteInFileInvalidLine()
        {
            Dictionary<int, string> lines = new Dictionary<int, string>();
            FileStructure fileStructure = new FileStructure("file", lines);
            List<FileStructure> file = new List<FileStructure>() { fileStructure };
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Write(currentPath, "file", "gh", "hah");

            Assert.AreEqual(expected, ">Invalid line");
        }

        [TestMethod]

        public void TestWriteInFileNotExist()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FileFunctions.Write(currentPath, "file", "4", "hah");

            Assert.AreEqual(expected, ">File not exist");
        }

        [TestMethod]

        public void TestWordCount()
        {
            Dictionary<int, string> lines = new Dictionary<int, string>() 
            {
                {1, "Zdravej sum Dimitar"}, {2, "Radwam se da se widim"}, {3, "Do utre"}
            };
            List<FileStructure> file = new List<FileStructure>();
            FileStructure fileStructure = new FileStructure("file.txt",lines);
            file.Add(fileStructure);
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            int wordCount = FileFunctions.WordCount(currentPath, "file.txt");

            Assert.AreEqual(wordCount, 10);
        }

        [TestMethod]

        public void TestWordCountEmptyFile()
        {
            Dictionary<int, string> lines = new Dictionary<int, string>();
            List<FileStructure> file = new List<FileStructure>();
            FileStructure fileStructure = new FileStructure("file.txt", lines);
            file.Add(fileStructure);
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            int wordCount = FileFunctions.WordCount(currentPath, "file.txt");

            Assert.AreEqual(wordCount, 0);
        }

        [TestMethod]

        public void TestWordCountFileNotExist()
        {
            Dictionary<int, string> lines = new Dictionary<int, string>();
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            int wordCount = FileFunctions.WordCount(currentPath, "file.txt");

            Assert.AreEqual(wordCount, 0);
        }
    }
}
