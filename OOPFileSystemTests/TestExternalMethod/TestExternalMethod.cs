﻿using OOPFileSystem;
using OOPFileSystem.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPFileSystemTests.TestExistMethod
{

    [TestClass]
    public class TestExternalMethod
    {
        [TestMethod]
        public void TestExistFolder()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure parent = new FolderStructure("home", folders, null, file);
            FolderStructure child = new FolderStructure("folder1", folders, parent, file);
            folders.Add(child);

            bool expect = Exist.Folder(child.Name, parent);

            Assert.IsTrue(expect);
        }

        [TestMethod]
        public void TestExistFilder()
        {
            List<FileStructure> files = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure parent = new FolderStructure("home", folders, null, files);
            FileStructure file = new FileStructure("file.txt", new Dictionary<int, string>());
            files.Add(file);

            bool expect = Exist.File(file.FileName, parent);

            Assert.IsTrue(expect);
        }

        [TestMethod]

        public void TestNameMerge()
        {
            string[] words = { "write", "file.txt", "1", "Az", "sum", "Dimitar" };
            int start = 3;

            string actual = Name.Merge(words, start);
            string expected = "Az sum Dimitar";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestNameMergeOneWord()
        {
            string[] words = { "Az"};
            int start = 0;

            string actual = Name.Merge(words, start);
            string expected = "Az";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestNameMergeNoWords()
        {
            string[] words = { };
            int start = 0;

            string actual = Name.Merge(words, start);
            string expected = "";

            Assert.AreEqual(expected, actual);
        }
    }
}
