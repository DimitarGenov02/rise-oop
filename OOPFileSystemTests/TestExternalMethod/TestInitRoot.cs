﻿using OOPFileSystem.Structures;
using OOPFileSystem;

namespace OOPFileSystemTests.TestExternalMethod
{
    [TestClass]
    public class TestInitRoot
    {
        private FolderStructure expected;
        private FolderStructure actual;
       [TestInitialize]
        public void SetUp()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();

            expected = new FolderStructure("home", folders, null!, file);
            actual = StageController.InitRoot();
        }

        [TestMethod]
        public void TestInitRootName()
        {
            Assert.AreEqual(expected.Name,actual.Name);
        }

        [TestMethod]
        public void TestInitRootChildren()
        {
            Assert.AreEqual(expected.Children.Count, actual.Children.Count);
        }

        [TestMethod]
        public void TestInitRootParent()
        {
            Assert.AreEqual(expected.Parent, actual.Parent);
        }

        [TestMethod]
        public void TestInitRootFile()
        {
            Assert.AreEqual(expected.Files.Count, actual.Files.Count);
        }
    }
}
