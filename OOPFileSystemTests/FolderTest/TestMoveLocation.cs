﻿using OOPFileSystem;
using OOPFileSystem.Structures;
using OOPFileSystem.FoldersModels;

namespace OOPFileSystemTests.FolderTest
{
    [TestClass]
    public class TestMoveLocation
    {

        [TestMethod]

        public void TestMoveLocationForward()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            FolderStructure actual = new FolderStructure("folder1", folders, currentPath, file);
            currentPath.Children.Add(actual);
            bool check = false;
            FolderStructure expected = FolderFunctions.MoveLocation(currentPath, "folder1", ref check);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]

        public void TestMoveLocationBackward()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            FolderStructure child = new FolderStructure("folder1", folders, currentPath, file);
            currentPath.Children.Add(child);
            bool check = false;
            FolderStructure expected = FolderFunctions.MoveLocation(child, "..", ref check);

            Assert.AreEqual(expected, currentPath);
        }

        [TestMethod]

        public void TestMoveLocationInvalid()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            bool expected = false;
            _ = FolderFunctions.MoveLocation(currentPath, "folder1", ref expected);

            Assert.IsTrue(expected);
        }
    }
}