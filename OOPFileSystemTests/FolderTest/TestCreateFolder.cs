﻿using OOPFileSystem;
using OOPFileSystem.Structures;
using OOPFileSystem.FoldersModels;

namespace OOPFileSystemTests.FolderTest
{
    [TestClass]
    public class TestCreateFolder
    {
        [TestMethod]
        public void TestCreate()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FolderFunctions.Create(currentPath, "name");


            Assert.AreEqual(expected, ">Folder created successfully");

        }
        [TestMethod]
        public void TestCreateFolderParent()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            var expected = currentPath.Parent;
            _ = FolderFunctions.Create(currentPath, "folder");

            Assert.AreEqual(expected, currentPath.Parent);
        }

        [TestMethod]

        public void TestCreateFolderEmptyName()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FolderFunctions.Create(currentPath, "");

            Assert.AreEqual(expected, ">Invalid name");
        }

        [TestMethod]

        public void TestCreateFolderInvalidName()
        {
            List<FileStructure> file = new List<FileStructure>();
            List<FolderStructure> folders = new List<FolderStructure>();
            FolderStructure currentPath = new FolderStructure("home", folders, null, file);

            string expected = FolderFunctions.Create(currentPath, "/name");

            Assert.AreEqual(expected, ">Invalid name");
        }
    }
}